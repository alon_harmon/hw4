#include "Vector.h"
/*
c' tor
*/
Vector::Vector(int n) {
	if (n < 2) {
		n = 2;
	}
	this->_elements = new int[n];
	this->_capacity = n;
	this->_size = 0;
	this->_resizeFactor = n;
}
// d'tor
Vector::~Vector() {
	if (this != NULL && this->_elements != NULL) {
		delete[] this->_elements;
		this->_elements = nullptr;
	}
}

// geters
int Vector::size() const {
	return this->_size;
}

int Vector::capacity() const {
	return this->_capacity;
}

int Vector::resizeFactor() const{
	return this->_resizeFactor;
}

// chack if empty
bool Vector::empty() const {
	return this->_size > 0;
}

// add val to elements from end and resise it
void Vector::push_back(const int& val) {  //problem here
	if (this->_capacity > this->_size) {
		this->_elements[this->_size] = val;
	}
	else {  // 
		int* new_elements = new int[this->_size + this->_resizeFactor];
		for (int i = 0; i < this->_size; i++) {
			new_elements[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = new_elements;
		this->_elements[this->_size] = val;
		this->_capacity = this->_size + this->_resizeFactor;

	}
	this->_size++;
}

// function return elemnt from end and resize it
int Vector::pop_back() {
	if (this->_size-1 <= 0) {
		std::cerr << "error: pop from empty vector" << std::endl;
		return -9999;
	}
	else {
		this->_size--;
		return this->_elements[this->_size];
	}
}


// function change capacity and reallocate if needed
void Vector::reserve(int n) {
	if (this->_capacity < n) {
		int div = this->_capacity - n / this->_resizeFactor;
		//this->_capacity += div * this->_resizeFactor;
		int* arr = this->_elements;
		this->_elements = new int[_capacity + _resizeFactor * ceil((n - _size) / _resizeFactor)]; ;
		for (int i = 0; i < this->_size; i++) {
			this->_elements[i] = arr[i];
		}
		delete[] arr;
		this->_capacity += _resizeFactor * ceil((n - _size) / _resizeFactor);
	}
}

// function resize elements
void Vector::resize(int n) {
	if (this->_capacity < n) {
		this->reserve(n);
	}

	this->_size = n;
}

// function assign value
void Vector::assign(int val) {
	for (int i = 0; i < this->_size; i++) {
		this->_elements[i] = val;
	}
}

// function resize vector
void Vector::resize(int n, const int& val) {
	this->resize(n);
	this->assign(val);
}

// function alawo acses to elements in vector ([] operator
int& Vector::operator[](int n) const {
	if (n < this->_size) {
		return this->_elements[n];
	}
	else {
		std::cerr << "overflow!!!" << std::endl;
		return this->_elements[0];
	}
}

// function alaw to dip copy vector
Vector& Vector::operator=(const Vector& other) {
	Vector new_vec = Vector(other.resizeFactor());
	new_vec.resize(other.capacity());
	new_vec.resize(other.size());
	for (int i = 0; i < new_vec.size(); i++) {  
		new_vec[i] = other[i];
	}
	return new_vec;
}

// function alaw to dip copy vector
Vector::Vector(const Vector& other) {
	(*this) = other;
}

// sum 2 vectors
Vector& Vector::operator+(const Vector& other) const {
	Vector new_vec = Vector(other);
	for (int i = 0; i < this->_size; i++) {
		new_vec[i] += this->_elements[i];
	}
	return new_vec;
}

// add vector to other
Vector& Vector::operator+=(const Vector& other) const {
	Vector new_vec = Vector(other);
	new_vec = new_vec + *this;
	return new_vec;
}

// 
Vector& Vector::operator-(const Vector& other) const {
	Vector new_vec = Vector(other);
	for (int i = 0; i < this->_size; i++) {
		new_vec[i] -= this->_elements[i];
	}
	return new_vec;
}

Vector& Vector::operator-=(const Vector& other) const {
	Vector new_vec = Vector(other);
	new_vec = new_vec - *this;
	return new_vec;
}

ostream& operator<<(ostream& os, const Vector& other)
{
	os << "Size is: " << other._size << std::endl << "Capacity is: " << other._capacity << std::endl << "Elements are: ";
	
	for (int i = 0; i < other._size; i++) {
		os << other[i] << " ";
	}
	os << std::endl;
	return os;
}